(function () {
    'use strict';

    angular.module('app.commons')
        .factory('ProduitService', ProduitService);

    ProduitService.$inject = ['$rootScope', '$http'];
    function ProduitService($rootScope, $http) {
        const chanelService = {};
        chanelService.createProduit = createProduit;
        chanelService.updateProduit = updateProduit;
        chanelService.enableDisableProduit = enableDisableProduit;
        chanelService.deleteProduit = deleteProduit;
        chanelService.getProduits = getProduits;
        chanelService.searchProduits = searchProduits;
        chanelService.getImagesProduit = getImagesProduit;

        return chanelService;

        function createProduit(p) {
            let request = {
                datasProduit: [p]
            };
            let URL = BASE_URL + '/produit/create';
            return $http.post(URL, request, {cache: true});
        }

        function updateProduit(p) {
            let request = {
                datasProduit: [p]
            };
            let URL = BASE_URL + '/produit/update';
            return $http.post(URL, request, {cache: true});
        }

        function enableDisableProduit(p) {
            let request = {
                datasProduit: [p]
            };
            let URL = BASE_URL + '/produit/update';
            return $http.post(URL, request, {cache: true});
        }

        function deleteProduit(p) {
            let request = {
                datasProduit: [p]
            };
            let URL = BASE_URL + '/produit/update';
            return $http.post(URL, request, {cache: true});
        }

        function getProduits(index, size) {
            let request = {
                dataProduit:{
                    idChanel: $rootScope.globals.currentUser.chanelUserInfos.idChanel,
                    trash: 0      //On recupère que les produits non supprimés
                    //status: 1   //On recupère que les produits actifs
                },
                index: index || 0,
                size: size || 6
            };
            let URL = BASE_URL + '/produit/getByCriteria';
            return $http.post(URL, request, {cache: true});
        }

        function searchProduits(criteria, index, size) {
            let request = {
                dataProduit: criteria,
                index: index || 0,
                size: size || 6
            };
            let URL = BASE_URL + '/produit/getByCriteria';
            return $http.post(URL, request, {cache: true});
        }

        function getImagesProduit(idProduit) {
            let request = {
                dataImageProduit: {
                    idProduit: idProduit
                }
            };
            let URL = BASE_URL + '/imageProduit/getByCriteria';
            return $http.post(URL, request, {cache: true});
        }
    }
})();