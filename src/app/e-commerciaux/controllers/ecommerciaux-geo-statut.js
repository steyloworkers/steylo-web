(function () {
    'use strict';

    angular.module('app.ecommerciaux')
        .controller('ecomGeoStatutCtrl', ecomGeoStatutCtrl);

    ecomGeoStatutCtrl.$inject = ['$rootScope', '$scope', 'demandeAbonneService'];
    function ecomGeoStatutCtrl($rootScope, $scope, demandeAbonneService) {

        $scope.getAbonneLocation = getAbonneLocation;
        $scope.getAbonneByStateLocalisation = getAbonneByStateLocalisation;
        $scope.getAbonneByStateLocalisation();

        function getAbonneByStateLocalisation(d) {
            switch (d){
                case 'en_attente':
                    $scope.getAbonneLocation(statut_demande.EN_ATTENTE);
                    break;
                case 'valide':
                    $scope.getAbonneLocation(statut_demande.VALIDE);
                    break;
                case 'rejete':
                    $scope.getAbonneLocation(statut_demande.REJETE);
                    break;
                default:
                    $scope.getAbonneLocation();
                    break;
            }
            //$rootScope.getDemandeAbonne();
        }

        function getAbonneLocation(demandeState) {
            console.log('demandeState', demandeState);
            demandeAbonneService.getAbonneLocation(demandeState)
                .then(function (res) {
                    console.log('response abonné', res);
                    if (!res.data.hasError){
                        if (res.data.count === 0){
                            console.log('res.data.count', res.data.count);
                            $scope.totalDemandeAbonne = res.data.count;
                            $scope.locations = [];
                            console.log('totalDemandeAbonne', $scope.totalDemandeAbonne);
                        }else {
                            $scope.locations = []; //Recoit la liste des localisations
                            $scope.demandesAbonneTemp = res.data.itemsAbonneDemande;
                            $scope.locations = _.map(res.data.itemsAbonneDemande, function (l) {
                                return [
                                    l.nomAbonne+' '+l.prenomAbonne,
                                    l.latitude,
                                    l.longitude
                                ]
                            });
                            $scope.totalDemandeAbonne = res.data.count;
                            console.log('totalDemandeAbonne', $scope.totalDemandeAbonne);
                            console.log('locations', $scope.locations);
                        }
                        getMap($scope.locations, demandeState);
                    }
                });
        }

        function getMap(locations, state) {
            /*/
            var locations = [
                ['Bondi Beach', -33.890542, 151.274856, 4],
                ['Coogee Beach', -33.923036, 151.259052, 5],
                ['Cronulla Beach', -34.028249, 151.157507, 3],
                ['Manly Beach', -33.80010128657071, 151.28747820854187, 2],
                ['Maroubra Beach', -33.950198, 151.259302, 1]
            ];
            //*/

            let center = locations.length === 0 ? new google.maps.LatLng(0.0, 0.0) : new google.maps.LatLng(locations[0][1], locations[0][2]);
            let map = new google.maps.Map(document.getElementById('mapStatut'), {
                zoom: 2,
                center: center,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                streetViewControl: false,
                fullscreenControl: false
            });

            let infowindow = new google.maps.InfoWindow();

            let marker, i;

            let icon;
            switch (state){
                case statut_demande.EN_ATTENTE:
                    icon = 'http://maps.google.com/mapfiles/ms/icons/yellow-dot.png';
                    break;
                case statut_demande.VALIDE:
                    icon = 'http://maps.google.com/mapfiles/ms/icons/green-dot.png';
                    break;
                case statut_demande.REJETE:
                    icon = 'http://maps.google.com/mapfiles/ms/icons/red-dot.png';
                    break;
                default:
                    icon = 'http://maps.google.com/mapfiles/ms/icons/blue-dot.png';
                    break;
            }

            if (locations !== null && locations.length > 0){
                for (i = 0; i < locations.length; i++) {
                    marker = new google.maps.Marker({
                        position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                        map: map,
                        animation: google.maps.Animation.DROP,
                        icon: icon
                    });

                    google.maps.event.addListener(marker, 'click', (function(marker, i) {
                        return function() {
                            infowindow.setContent(locations[i][0]);
                            infowindow.open(map, marker);
                        }
                    })(marker, i));
                }
            }
        }
    }
})();