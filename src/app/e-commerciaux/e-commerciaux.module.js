(function () {
    'use strict';

    angular.module('app.ecommerciaux', [
        'app.commons',
        /*/
        'oc.lazyLoad',
        'ncy-angular-breadcrumb',
        'angular-loading-bar',
        //'ngMaterial',
        //'mdSteppers',

        //*/
    ])
        .config(['$stateProvider', '$urlRouterProvider', '$ocLazyLoadProvider', '$breadcrumbProvider', function ($stateProvider, $urlRouterProvider, $ocLazyLoadProvider, $breadcrumbProvider) {

            $ocLazyLoadProvider.config({
                // Set to true if you want to see what and when is dynamically loaded
                debug: true
            });

            $urlRouterProvider.otherwise('/login');
            $stateProvider
                .state('app.ecommerciaux', {
                    url: '/e-commerciaux',
                    templateUrl: 'app/e-commerciaux/views/e-commerciaux.html',
                    ncyBreadcrumb: {
                        label: 'Gestion des E-commerciaux'
                    },
                    controller: 'ecomCtrl',
                    resolve: {
                        loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                            // you can lazy load controllers
                            return $ocLazyLoad.load({
                                files: [
                                    'app/e-commerciaux/controllers/ecommerciauxCtrl.js',
                                ]
                            });
                        }],
                        /*loadDemandeAbonne: ['demandeAbonneService', 'commonUtilities', 'lodash', function (demandeAbonneService, commonUtilities, lodash) {
                            return demandeAbonneService.getAbonneDemande(statut_demande.EN_ATTENTE)
                                .then(function (res) {
                                    let demandesAbonneInfo = {};
                                    if (!res.data.hasError){
                                        demandesAbonneInfo.demandesAbonne = res.data.itemsAbonneDemande;
                                        lodash.forEach(demandesAbonneInfo.demandesAbonne, function (ad) {
                                            if (ad.photoAbonne){
                                                //data:image/png;base64,
                                                ad.photoAbonne = commonUtilities.getImageFromBase64(ad)
                                            }
                                            demandesAbonneInfo.demandesAbonne.push(ad);
                                        });
                                        demandesAbonneInfo.totalDemandeAbonne = demandesAbonneInfo.demandesAbonne.length;
                                        console.log('demandesAbonne', demandesAbonneInfo.demandesAbonne);
                                        console.log('totalDemandeAbonne', demandesAbonneInfo.totalDemandeAbonne);
                                    }
                                    return demandesAbonneInfo;
                                });
                        }]*/
                    },
                    redirectTo: 'app.ecommerciaux.demandeurs',  //permet de faire une redirection à la sous vue
                })
                .state('app.ecommerciaux.demandeurs', {
                    url: '/demandeurs',
                    ncyBreadcrumb: {
                        label: 'Demandeurs'
                    },
                    views: {
                        'ec_sub_menu': {
                            templateUrl: 'app/e-commerciaux/views/abonne/abonne-infos.html',
                            controller: 'ecomCtrl'
                        },
                    },
                })
                .state('app.ecommerciaux.demandeurs.details', {
                    url: '/J1RhaGEgQW1vdXNzb3UgUGhpbG9tw6huZSc=:idAbonne&:idDemande&J04nZ3Vlc3NhbiBJbm5vY2VudCc=',

                    views: {
                        'abonne_info': {
                            templateUrl: 'app/e-commerciaux/views/abonne/abonne-infos-details.html',
                            controller: 'ecomDetailsCtrl'
                        }
                    },
                    ncyBreadcrumb: {
                        label: 'Détails'
                    }
                })
                .state('app.ecommerciaux.geolocalisationStatut', {
                    url: '/geo-par-statut',
                    ncyBreadcrumb: {
                        label: 'Géolocalisation par statut'
                    },
                    views: {
                        'ec_sub_menu': {
                            templateUrl: 'app/e-commerciaux/views/abonne/by-etat-demandeurs.html',
                            controller: 'ecomGeoStatutCtrl'
                        }
                    },

                })
                .state('app.ecommerciaux.geolocalisationLocalite', {
                    url: '/geo-par-localite',
                    ncyBreadcrumb: {
                        label: 'Géolocalisation par localité'
                    },
                    views: {
                        'ec_sub_menu': {
                            templateUrl: 'app/e-commerciaux/views/abonne/by-localite-demandeurs.html',
                            controller: 'ecomGeoLocaliteCtrl'
                        }
                    },

                })
        }])
})();