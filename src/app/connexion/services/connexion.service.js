(function () {
    'use strict';

    angular.module('app')
        .factory('ConnexionService', ConnexionService);

    ConnexionService.$inject = ['$rootScope', '$http', '$cookies', 'localStorageService'];
    function ConnexionService($rootScope, $http, $cookies, localStorageService) {
        const connexionService = {};
        connexionService.login = login;
        connexionService.logout = logout;
        connexionService.updateChanelUser = updateChanelUser;
        connexionService.updatePassChanelUser = updatePassChanelUser;
        connexionService.setCredentials = setCredentials;
        connexionService.clearCredentials = clearCredentials;
        return connexionService;

        function login(user) {
            let request = {
                dataChanelUsers: user
            };
            let URL = BASE_URL + '/chanelUsers/connexion';
            return $http.post(URL, request, {cache: true});
        }

        function logout(id) {
            let request = {
                dataChanelUsers: {
                    id: id
                }
            };
            let URL = BASE_URL + '/chanelUsers/deconnexion';
            return $http.post(URL, request, {cache: true});
        }

        function setCredentials(userInfos) {
            //let authdata = commonUtilities.base64.encode();

            let chanelUserInfos = {
                dateCreation: userInfos.dateCreation,
                email: userInfos.email,
                id: userInfos.id,
                idChanel: userInfos.idChanel,
                login: userInfos.login,
                nom: userInfos.nom,
                nomChanel: userInfos.nomChanel,
                password: userInfos.password,
                prenoms: userInfos.prenoms,
                statut: userInfos.statut,
                updatedAt: userInfos.updatedAt
            };

            let logoChanel = {
                logo: userInfos.logoChanel,
                //extLogo: userInfos.extLogo
            };

            $rootScope.globals = {
                currentUser: {
                    chanelUserInfos
                }
            };

            $rootScope.globalsLogo = {
                currentLogo: {
                    logoChanel
                }
            };

            console.log('currentUser', $rootScope.globals);
            console.log('currentLogo', $rootScope.globalsLogo);

            // set default auth header for http requests
            //$http.defaults.headers.common['Authorization'] = 'Basic ' + authdata;

            // store user details in globals cookie that keeps user logged in for 1 week (or until they logout)
            let cookieExp = new Date();
            cookieExp.setDate(cookieExp.getDate() + 7);
            $cookies.putObject('globals-steylo', $rootScope.globals, { expires: cookieExp });

            localStorageService.set('globals-steylo-logo', $rootScope.globalsLogo);
        }

        function clearCredentials() {
            $rootScope.globals = {};
            $rootScope.globalsLogo = {};
            $cookies.remove('globals-steylo');
            localStorageService.remove('globals-steylo-logo');
            //$http.defaults.headers.common.Authorization = 'Basic';
        }

        function updateChanelUser(user) {
            let request = {
                datasChanelUsers: [user]
            };
            let URL = BASE_URL + '/chanelUsers/update';
            return $http.post(URL, request, {cache: true});
        }
        
        function updatePassChanelUser(user) {
            let request = {
                dataChanelUsers: user
            };
            let URL = BASE_URL + '/chanelUsers/changePass';
            return $http.post(URL, request, {cache: true});
        }
    }
})();