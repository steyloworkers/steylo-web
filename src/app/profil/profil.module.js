(function () {
    'use strict';

    angular.module('app.profil', [
        'app.commons'
        /*/
        'oc.lazyLoad',
        'ncy-angular-breadcrumb',
        'angular-loading-bar',
        //'ngMaterial',
        //'mdSteppers',

        //*/
    ])
        .config(['$stateProvider', '$urlRouterProvider', '$ocLazyLoadProvider', '$breadcrumbProvider', function ($stateProvider, $urlRouterProvider, $ocLazyLoadProvider, $breadcrumbProvider) {

            $ocLazyLoadProvider.config({
                // Set to true if you want to see what and when is dynamically loaded
                debug: true
            });

            $urlRouterProvider.otherwise('/login');
            $stateProvider
                .state('app.profil', {
                    url: '/profil',
                    templateUrl: 'app/profil/views/profil.html',
                    controller: 'profilCtrl',
                    ncyBreadcrumb: {
                        label: 'Profil',
                    },
                    resolve: {
                        loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                            // you can lazy load controllers
                            return $ocLazyLoad.load({
                                files: [
                                    'app/profil/controllers/profilCtrl.js',
                                ]
                            });
                        }]
                    }
                })
        }])
})();