(function () {
    'use strict';

    angular.module('app.commons')
        .factory('secteurActiviteService', secteurActiviteService);

    secteurActiviteService.$inject = ['$http'];
    function secteurActiviteService($http){
        let secteurActiviteService = {};

        secteurActiviteService.getSecteurActivite = getSecteurActivite;

        return secteurActiviteService;

        function getSecteurActivite() {
            let req = {
                dataSecteurActivite: {}
            };
            let URL = BASE_URL + '/secteurActivite/getByCriteria';
            return $http.post(URL, req, {cache: true});
        }
    }
})();