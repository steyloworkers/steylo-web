(function () {
    'use strict';

    angular.module('app.commons')
        .factory('SousCategoriesService', SousCategoriesService);

    SousCategoriesService.$inject = ['$http'];
    function SousCategoriesService($http) {
        const sousCategoriesService = {};
        sousCategoriesService.getSousCategories = getSousCategories;

        return sousCategoriesService;

        function getSousCategories(idCategorie) {
            let request = {
                dataSousCategorieProduit: {
                    idCategorie: idCategorie
                }
            };
            let URL = BASE_URL + '/sousCategorieProduit/getByCriteria';
            return $http.post(URL, request, {cache: true});
        }
    }
})();