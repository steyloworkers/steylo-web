(function () {
    'use strict';

    angular.module('app.commons')
        .factory('CategoriesService', CategoriesService);

    CategoriesService.$inject = ['$http'];
    function CategoriesService($http) {
        const categoriesService = {};
        categoriesService.getCategories = getCategories;

        return categoriesService;

        function getCategories(idMarque) {
            let request = {
                dataCategorieProduit: {
                    idMarque: idMarque ? idMarque : ''
                }
            };
            let URL = idMarque ? BASE_URL + '/categorieProduit/getCategorieByMarque' : BASE_URL + '/categorieProduit/getByCriteria';
            return $http.post(URL, request, {cache: true});
        }
    }
})();