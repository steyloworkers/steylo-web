(function () {
    'use strict';

    angular.module('app.commons')
        .factory('MarqueService', MarqueService);

    MarqueService.$inject = ['$http'];
    function MarqueService($http) {
        const marqueService = {};
        marqueService.getMarque = getMarque;

        return marqueService;

        function getMarque() {
            let request = {
                dataMarque: {}
            };
            let URL = BASE_URL + '/marque/getByCriteria';
            return $http.post(URL, request, {cache: true});
        }
    }
})();