angular
.module('app.commons')
.directive('includeReplace', includeReplace)
.directive('a', preventClickDirective)
.directive('a', bootstrapCollapseDirective)
.directive('a', navigationDirective)
.directive('button', layoutToggleDirective)
.directive('button', minimizedToggleDirective)
.directive('a', layoutToggleDirective)
.directive('button', collapseMenuTogglerDirective)
.directive('div', bootstrapCarouselDirective)
.directive('toggle', bootstrapTooltipsPopoversDirective)
.directive('tab', bootstrapTabsDirective)
.directive('button', cardCollapseDirective)
.directive('loading', loading)
.directive('enterKey', enterKeyDirective)
.directive('widgetDropzone', widgetDropzone)
.directive('upperCase', upperCase)
.directive('appFilereader', appFilereader)
.directive('pagination', paginationDirective)
.directive('cannotCopyCutPaste', cannotCopyCutPaste)
.directive('limitCharTo', limitCharTo)
.directive('numberOnly', numberOnly)
.directive('onlyAlphaNumeric', onlyAlphaNumeric)

function includeReplace() {
  let directive = {
    require: 'ngInclude',
    restrict: 'A',
    link: link
  }
  return directive;

  function link(scope, element, attrs) {
    element.replaceWith(element.children());
  }
}

//Prevent click if href="#"
function preventClickDirective() {
  let directive = {
    restrict: 'E',
    link: link
  }
  return directive;

  function link(scope, element, attrs) {
    if (attrs.href === '#'){
      element.on('click', function(event){
        event.preventDefault();
      });
    }
  }
}

//Bootstrap Collapse
function bootstrapCollapseDirective() {
  let directive = {
    restrict: 'E',
    link: link
  }
  return directive;

  function link(scope, element, attrs) {
    if (attrs.toggle=='collapse'){
      element.attr('href','javascript;;').attr('data-target',attrs.href.replace('index.html',''));
    }
  }
}

/**
* @desc Genesis main navigation - Siedebar menu
* @example <li class="nav-item nav-dropdown"></li>
*/
function navigationDirective() {
  let directive = {
    restrict: 'E',
    link: link
  }
  return directive;

  function link(scope, element, attrs) {
    if(element.hasClass('nav-dropdown-toggle') && angular.element('body').width() > 782) {
      element.on('click', function(){
        if(!angular.element('body').hasClass('compact-nav')) {
          element.parent().toggleClass('open').find('.open').removeClass('open');
        }
      });
    } else if (element.hasClass('nav-dropdown-toggle') && angular.element('body').width() < 783) {
      element.on('click', function(){
        element.parent().toggleClass('open').find('.open').removeClass('open');
      });
    }
  }
}

//Dynamic resize .sidebar-nav
sidebarNavDynamicResizeDirective.$inject = ['$window', '$timeout'];
function sidebarNavDynamicResizeDirective($window, $timeout) {
  let directive = {
    restrict: 'E',
    link: link
  }
  return directive;

  function link(scope, element, attrs) {

    if (element.hasClass('sidebar-nav') && angular.element('body').hasClass('fixed-nav')) {
      let bodyHeight = angular.element(window).height();
      scope.$watch(function(){
        let headerHeight = angular.element('header').outerHeight();

        if (angular.element('body').hasClass('sidebar-off-canvas')) {
          element.css('height', bodyHeight);
        } else {
          element.css('height', bodyHeight - headerHeight);
        }
      })

      angular.element($window).bind('resize', function(){
        let bodyHeight = angular.element(window).height();
        let headerHeight = angular.element('header').outerHeight();
        let sidebarHeaderHeight = angular.element('.sidebar-header').outerHeight();
        let sidebarFooterHeight = angular.element('.sidebar-footer').outerHeight();

        if (angular.element('body').hasClass('sidebar-off-canvas')) {
          element.css('height', bodyHeight - sidebarHeaderHeight - sidebarFooterHeight);
        } else {
          element.css('height', bodyHeight - headerHeight - sidebarHeaderHeight - sidebarFooterHeight);
        }
      });
    }
  }
}

//LayoutToggle
layoutToggleDirective.$inject = ['$interval'];
function layoutToggleDirective($interval) {
  let directive = {
    restrict: 'E',
    link: link
  }
  return directive;

  function link(scope, element, attrs) {
    element.on('click', function(){

      if (element.hasClass('sidebar-toggler')) {
        angular.element('body').toggleClass('sidebar-hidden');
      }

      if (element.hasClass('aside-menu-toggler')) {
        angular.element('body').toggleClass('aside-menu-hidden');
      }
    });
  }
}

//MinimizedToggle
minimizedToggleDirective.$inject = ['$interval'];
function minimizedToggleDirective($interval) {
    let directive = {
        restrict: 'E',
        link: link
    }
    return directive;

    function link(scope, element, attrs) {
        element.on('click', function(){
            if (element.hasClass('minimized-toggler') && angular.element('body').hasClass('sidebar-compact')) {
                angular.element('body').removeClass('sidebar-compact');
                angular.element('body').toggleClass('sidebar-minimized');
                console.log('11111111111111');
            }
            else if (element.hasClass('minimized-toggler') && !angular.element('body').hasClass('sidebar-compact')) {
                //angular.element('body').addClass('sidebar-compact');
                angular.element('body').toggleClass('sidebar-compact');
                angular.element('body').removeClass('sidebar-minimized');
                //angular.element('body').toggleClass('sidebar-minimized');
                console.log('222222222222222');
            }
        });
    }
}

//Collapse menu toggler
function collapseMenuTogglerDirective() {
  let directive = {
    restrict: 'E',
    link: link
  }
  return directive;

  function link(scope, element, attrs) {
    element.on('click', function(){
      if (element.hasClass('navbar-toggler') && !element.hasClass('layout-toggler')) {
        angular.element('body').toggleClass('sidebar-mobile-show')
      }
    })
  }
}

//Bootstrap Carousel
function bootstrapCarouselDirective() {
  let directive = {
    restrict: 'E',
    link: link
  }
  return directive;

  function link(scope, element, attrs) {
    if (attrs.ride=='carousel'){
      element.find('a').each(function(){
        $(this).attr('data-target',$(this).attr('href').replace('index.html','')).attr('href','javascript;;')
      });
    }
  }
}

//Bootstrap Tooltips & Popovers
function bootstrapTooltipsPopoversDirective() {
  let directive = {
    restrict: 'A',
    link: link
  }
  return directive;

  function link(scope, element, attrs) {
    if (attrs.toggle=='tooltip'){
      angular.element(element).tooltip();
    }
    if (attrs.toggle=='popover'){
      angular.element(element).popover();
    }
  }
}

//Bootstrap Tabs
function bootstrapTabsDirective() {
  let directive = {
    restrict: 'A',
    link: link
  }
  return directive;

  function link(scope, element, attrs) {
    element.click(function(e) {
      e.preventDefault();
      angular.element(element).tab('show');
    });
  }
}

//Card Collapse
function cardCollapseDirective() {
  let directive = {
    restrict: 'E',
    link: link
  }
  return directive;

  function link(scope, element, attrs) {
    if (attrs.toggle=='collapse' && element.parent().hasClass('card-actions')){

      if (element.parent().parent().parent().find('.card-block').hasClass('in')) {
        element.find('i').addClass('r180');
      }

      let id = 'collapse-' + Math.floor((Math.random() * 1000000000) + 1);
      element.attr('data-target','#'+id)
      element.parent().parent().parent().find('.card-block').attr('id',id);

      element.on('click', function(){
        element.find('i').toggleClass('r180');
      })
    }
  }
}

function loading() {
    return {
        restrict: 'E',
        replace:true,
        template: '<p><img src="img/images/loader-128x/Preloader_2.gif"/></p>', // Define a template where the image will be initially loaded while waiting for the ajax request to complete
        link: function (scope, element, attr) {
            scope.$watch('loading', function (val) {
                val = val ? $(element).show() : $(element).hide();  // Show or Hide the loading image
            });
        }
    }
}

//Permet de valider avec la touche "entrée"
function enterKeyDirective() {
    return function(scope, element, attrs) {
        element.bind("keydown keypress", function(event) {
            if (event.which === 13) {
                scope.$apply(function() {
                    scope.$eval(attrs.enterKey);
                });

                event.preventDefault();
            }
        });
    };
}

function widgetDropzone() {
    return {
        restrict: "E",
        scope: {
            options: "="
        },
        controller: "WidgetDropzoneCtrl",
        templateUrl: "app/widgets/dropzone/dropzone.html",
        replace: true
    };
}

upperCase.$inject = ['commonUtilities'];
function upperCase(commonUtilities) {
    return {
        require: "ngModel",
        link: function (scope, element, attrs, modelCtrl) {
            let capitalize = function (inputValue) {
                if (!commonUtilities.isNullOrEmptyValue(inputValue)) {
                    let capitalized = inputValue.toUpperCase();
                    if (capitalized !== inputValue) {
                        modelCtrl.$setViewValue(capitalized);
                        modelCtrl.$render();
                    }
                    return capitalized;
                }
            };
            modelCtrl.$parsers.push(capitalize);
            capitalize(scope[attrs.ngModel]);
        }
    };
}

appFilereader.$inject = ['$rootScope', '$q'];
function appFilereader($rootScope, $q) {
    let slice = Array.prototype.slice;

    return {
        restrict: "A",
        require: "?ngModel",
        link: function (scope, element, attrs, ngModel) {

            if (!ngModel) return;

            ngModel.$render = function () { };

            element.bind("change", function (e) {
                let element = e.target;
                if (!element.value) return;

                let eventNameToListenTo = !element.id ? "changed" : element.id + "-changed";

                $rootScope.$emit(eventNameToListenTo, { detail: { fileName: element.value} });

                element.disabled = true;
                $q.all(slice.call(element.files, 0).map(readFile))
                    .then(function (values) {
                        console.log("afterRead",values);
                        if (element.multiple) ngModel.$setViewValue(values);
                        else ngModel.$setViewValue(values.length ? values[0] : null);
                        //element.value = null;
                        element.disabled = false;
                    });

                function readFile(file) {
                    let deferred = $q.defer();

                    let reader = new FileReader();
                    reader.onload = function (e) {
                        deferred.resolve(e.target.result);
                    };
                    reader.onerror = function (e) {
                        deferred.reject(e);
                    };

                    $rootScope.photoName = file.name.split(".")[0];
                    //console.log("file",file);
                    $rootScope.filename =
                        {
                            imgName:file.name.split(".")[0],
                            type: file.type.split("/")[1]
                        };


                    //console.log("file sous",element.id);
                    reader.readAsDataURL(file);

                    return deferred.promise;
                }

            }); //change

        } //link
    }; //return
}

function paginationDirective() {
    return {
        restrict: 'E',
        scope: {
            numPages: '=',
            currentPage: '=',
            onSelectPage: '&'
        },

        templateUrl: 'app/commons/layout/pagination.html',
        replace: true,
        link: function(scope) {
            scope.$watch('numPages', function(value) {
                scope.pages = [];
                for (let i = 1; i <= value; i++) {
                    scope.pages.push(i);
                }
                if (scope.currentPage > value) {
                    scope.selectPage(value);
                }
            });
            scope.noPrevious = function() {
                return scope.currentPage === 1;
            };
            scope.noNext = function() {
                return scope.currentPage === scope.numPages;
            };
            scope.isActive = function(page) {
                return scope.currentPage === page;
            };

            scope.selectPage = function(page) {
                if (!scope.isActive(page)) {
                    scope.currentPage = page;
                    scope.onSelectPage({ page: page });
                }
            };

            scope.selectPrevious = function() {
                if (!scope.noPrevious()) {
                    scope.selectPage(scope.currentPage - 1);
                }
            };
            scope.selectNext = function() {
                if (!scope.noNext()) {
                    scope.selectPage(scope.currentPage + 1);
                }
            };
        }
    };

}

function cannotCopyCutPaste() {
    return {
        scope: {},
        link:function(scope,element){
            element.on('cut copy paste', function (event) {
                event.preventDefault();
            });
        }
    };
}

function limitCharTo() {
    return {
        restrict: "A",
        link: function(scope, elem, attrs) {
            let limit = parseInt(attrs.limitCharTo);
            angular.element(elem).on('keypress', function(e) {
                if (this.value.length === limit) e.preventDefault();
            });
        }
    }
}

function numberOnly() {
    return {
        require: 'ngModel',
        link: function (scope, element, attr, ngModelCtrl) {
            function fromUser(text) {
                let transformedInput = text.replace(/[^0-9]/g, '');
                if (transformedInput !== text) {
                    ngModelCtrl.$setViewValue(transformedInput);
                    ngModelCtrl.$render();
                }
                return transformedInput;
            }
            ngModelCtrl.$parsers.push(fromUser);
        }
    };
}

function onlyAlphaNumeric() {
    return {
        require: 'ngModel',
        link: function(scope, element, attr, ngModelCtrl) {
            function fromUser(text) {
                let transformedInput = text.replace(/[^a-zA-Z0-9]/g, '');
                if (transformedInput !== text) {
                    ngModelCtrl.$setViewValue(transformedInput);
                    ngModelCtrl.$render();
                }
                return transformedInput;
            }
            ngModelCtrl.$parsers.push(fromUser);
        }
    };
}