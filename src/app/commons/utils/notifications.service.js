(function () {
    'use strict';

    angular.module('app.commons')
        .factory('notificationService', notificationService);

    notificationService.$inject = [];
    function notificationService(){
        let notificationService = {};

        //Configuration avec le thème flatty
        let flatty = humane.create({baseCls: 'humane-flatty', timeout: 3000});
        flatty.error = flatty.spawn({addnCls: 'humane-flatty-error'});
        flatty.success = flatty.spawn({addnCls: 'humane-flatty-success'});
        //flatty.warning = flatty.spawn({addnCls: 'humane-flatty-warning'});

        //Configuration avec le thème libnotify
        let libnotify = humane.create({baseCls: 'humane-libnotify', timeout: 3000});
        libnotify.error = libnotify.spawn({addnCls: 'humane-libnotify-error'});
        libnotify.success = libnotify.spawn({addnCls: 'humane-libnotify-success'});

        //*/
        //Configuration avec le thème jackedup
        let jackedup = humane.create({baseCls: 'humane-jackedup', timeout: 3000});
        jackedup.error = jackedup.spawn({addnCls: 'humane-jackedup-error'});
        jackedup.success = jackedup.spawn({addnCls: 'humane-jackedup-success'});
        //*/

        notificationService.flatty = flatty;
        notificationService.libnotify = libnotify;
        notificationService.jackedup = jackedup;

        return notificationService;
    }
})();