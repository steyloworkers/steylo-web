(function () {
    'use strict';

    angular.module('app.parametres', [
        'ui.router',
        //*/
        'oc.lazyLoad',
        'ncy-angular-breadcrumb',
        'angular-loading-bar',
        //'ngMaterial',
        //'mdSteppers',

        //*/
    ])
        .config(['$stateProvider', '$urlRouterProvider', '$ocLazyLoadProvider', '$breadcrumbProvider', function ($stateProvider, $urlRouterProvider, $ocLazyLoadProvider, $breadcrumbProvider) {

            $ocLazyLoadProvider.config({
                // Set to true if you want to see what and when is dynamically loaded
                debug: true
            });

            $urlRouterProvider.otherwise('/login');
            $stateProvider
                .state('app.parametres', {
                    url: '/parametres',
                    templateUrl: 'app/parametres/views/parametres.html',
                    ncyBreadcrumb: {
                        label: 'Paramètres'
                    },
                    controller: 'paramCtrl',
                    resolve: {
                        loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                            // you can lazy load controllers
                            return $ocLazyLoad.load({
                                files: ['app/parametres/controllers/parametresCtrl.js']
                            });
                        }]
                    }
                })
        }])
})();