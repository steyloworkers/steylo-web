(function () {
    'use strict';

    //main.js
    angular
        .module('app')
        .controller('dashboardCtrl', dashboardCtrl);

    dashboardCtrl.$inject = ['$scope'];
    function dashboardCtrl($scope){

        $scope.jsTree = jsTree();

        function jsTree() {
            $scope.treeData = [];
            let newId = 1;
            $scope.ignoreChanges = false;
            $scope.newNode = {};
            //$scope.treeData = $scope.listeFonctionnalite;
            $scope.treeData = [ //A remplacer avec les données d'un web service
                {
                    "id": "ajson1",
                    "parent": "#",
                    "text": "Simple root node",
                    "state": {
                        "opened": true
                    },
                    "__uiNodeId": 6
                },
                {
                    "id": "ajson2",
                    "parent": "#",
                    "text": "Root node 2",
                    "state": {
                        "opened": true
                    },
                    "__uiNodeId": 7
                },
                {
                    "id": "ajson3",
                    "parent": "ajson2",
                    "text": "Child 1",
                    "state": {
                        "opened": true
                    },
                    "__uiNodeId": 8
                },
                {
                    "id": "ajson4",
                    "parent": "ajson2",
                    "text": "Child 2",
                    "state": {
                        "opened": true
                    },
                    "__uiNodeId": 9
                },
                {
                    "id": "2",
                    "parent": "ajson1",
                    "text": "Async Loaded",
                    "__uiNodeId": 10,
                    "type": "cloud"
                }
            ];
            $scope.treeConfig = {
                core: {
                    multiple: true,
                    animation: true,
                    error: function(error) {
                        console.log('treeCtrl: error from js tree - ' + angular.toJson(error));
                    },
                    check_callback: true,
                    worker: true
                },
                types: {
                    default: {
                        icon: 'fa fa-user'
                    },
                    star: {
                        icon: 'fa fa-user'
                    },
                    cloud: {
                        icon: 'fa fa-user'
                    }
                },
                version: 1,
                plugins: ['types', 'checkbox']
            };
        }
    }
})();