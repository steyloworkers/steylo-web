(function () {
    'use strict';

    angular.module('app.suivis', [
        'ui.router',
        //*/
        'oc.lazyLoad',
        'ncy-angular-breadcrumb',
        'angular-loading-bar',
        //'ngMaterial',
        //'mdSteppers',

        //*/
    ])
        .config(['$stateProvider', '$urlRouterProvider', '$ocLazyLoadProvider', '$breadcrumbProvider', function ($stateProvider, $urlRouterProvider, $ocLazyLoadProvider, $breadcrumbProvider) {

            $ocLazyLoadProvider.config({
                // Set to true if you want to see what and when is dynamically loaded
                debug: true
            });

            $urlRouterProvider.otherwise('/login');
            $stateProvider
                .state('app.suivis', {
                    url: '/suivis',
                    templateUrl: 'app/suivis/views/suivis.html',
                    ncyBreadcrumb: {
                        label: 'Suivis'
                    },
                    controller: 'suivisCtrl',
                    resolve: {
                        loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                            // you can lazy load controllers
                            return $ocLazyLoad.load({
                                files: ['app/suivis/controllers/suivisCtrl.js']
                            });
                        }]
                    }
                })
        }])
})();