(function () {
    'use strict';

    angular.module('app.utilisateurs', [
        'ui.router',
        //*/
        'oc.lazyLoad',
        'ncy-angular-breadcrumb',
        'angular-loading-bar',
        //'ngMaterial',
        //'mdSteppers',

        //*/
    ])
        .config(['$stateProvider', '$urlRouterProvider', '$ocLazyLoadProvider', '$breadcrumbProvider', function ($stateProvider, $urlRouterProvider, $ocLazyLoadProvider, $breadcrumbProvider) {

            $ocLazyLoadProvider.config({
                // Set to true if you want to see what and when is dynamically loaded
                debug: true
            });

            $urlRouterProvider.otherwise('/login');
            $stateProvider
                .state('app.utilisateurs', {
                    url: '/utilisateurs',
                    templateUrl: 'app/utilisateurs/views/utilisateurs.html',
                    ncyBreadcrumb: {
                        label: 'Utilisateurs'
                    },
                    controller: 'suivisCtrl',
                    resolve: {
                        loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                            // you can lazy load controllers
                            return $ocLazyLoad.load({
                                files: ['app/utilisateurs/controllers/utilisateursCtrl.js']
                            });
                        }]
                    }
                })
        }])
})();