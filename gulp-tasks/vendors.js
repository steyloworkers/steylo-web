'use strict';

let gulp = require('gulp');
let del = require('del');
let rename = require('gulp-rename');
let runSequence = require('run-sequence');
let sass = require('gulp-sass');
let autoprefixer = require('gulp-autoprefixer');
let cssmin = require('gulp-cssmin');

let paths = gulp.paths;

gulp.task('compile-vendors:clean', function () {
  return del([
    paths.src + 'vendors/css/**'
  ]);
});

gulp.task('compile-vendors:sass', function () {
  return gulp.src(paths.src + 'scss/vendors/**/*.scss')
  .pipe(sass().on('error', sass.logError))
  .pipe(autoprefixer())
  .pipe(rename({dirname: ''}))
  .pipe(gulp.dest(paths.src + 'vendors/css/'))
  .pipe(cssmin())
  .pipe(rename({suffix: '.min'}))
  .pipe(rename({dirname: ''}))
  .pipe(gulp.dest(paths.src + 'vendors/css/'));
});

gulp.task('compile-vendors', function(callback) {
  runSequence('compile-vendors:clean', 'compile-vendors:sass', callback);
});
